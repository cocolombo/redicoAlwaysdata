from django.conf.urls.defaults import *
from redico.redicos.models import Redico
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
# from redico.redicos.views import redico, redicoProps
urlpatterns = patterns('redico.redicos.views',
    #REDICOS===ajout/un/tous=============================================
    # Un nouveau redico
    # redicos/ajout 
    url('^ajout/$',                  
                                'ajout'),

    # Editer un redico existant 
    # redicos/edit/1 
    url('^edit/(?P<redico_id>\d+)/$', 
                                'edit'),

    
    # Toutes les props d'un redico
    # redico/1 
    url('^(?P<redico_id>\d+)/$',      
                                'detail'),                 
    # redico/1/statsdetail 
    url('^(?P<redico_id>\d+)/(?P<statsdetail>statsdetail)/$',      
                                'detail'),                 

    
    # La liste de tous les redicos
    # redico/ 
    url('^$',   
                                 'index'),
)