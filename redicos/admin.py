from django.contrib import admin
from redico.redicos.models import Redico

#admin.site.disable_action('delete_selected')

class RedicoAdmin(admin.ModelAdmin):
    fields         = ('titre',)
    list_display   = ('titre', )
    #list_filter    = ('createur',)
    #ordering       = ('debut',)
    search_fields  = ('title',)
    #date_hierarchy = 'debut'
    actions = ['redico_action']
    #inlines = [PropChoiceInline]
    
    def redico_action(self, request, queryset):
        pass
        #rows_updated = queryset.update(status='p')
        #if rows_updated == 1:
        #   message_bit = "1 story was"
        #else:
        #   message_bit = "%s stories were" % rows_updated
        #self.message_user(request, "%s successfully marked as published." % message_bit)
        self.message_user(request, "Action sur model redico")

admin.site.register(Redico, RedicoAdmin)



