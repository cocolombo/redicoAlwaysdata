from django.db import models
from django.contrib.auth.models import User
from redico.joueurs.models import Joueur
# Create your models here.

class Redico(models.Model):
    titre        = models.TextField(unique=True)
    createur     = models.ForeignKey(User, editable=False, related_name='Redico_createur')
    debut        = models.DateTimeField(auto_now_add=True)
    actif        = models.BooleanField(default=True, editable=False)

    def __unicode__(self):
        return u"%s" % (self.titre)           
            
    def get_absolute_url(self):

        return "/redico/%i/" % self.id

      
