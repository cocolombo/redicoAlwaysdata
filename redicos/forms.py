from django.forms import ModelForm, Textarea
from redico.redicos.models import Redico

class RedicoForm(ModelForm):
 
    class Meta:
        model = Redico
        fields  = ('titre',) # 'createur', 'debut')
        widgets = {
            'titre': Textarea(attrs={'cols': 60, 'rows': 3}),
        }
