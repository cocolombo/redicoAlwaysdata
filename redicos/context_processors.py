from redico.propositions.models import Proposition
from redico.evaluations.models import Evaluation
from redico.redicos.models import Redico
from django.db.models import Count

def joueurs_actifs(request): 
    
    try:
        joueurs_actifs = Evaluation.objects.all().values('joueur__username', 'joueur__id').annotate(cnt=Count('joueur__username')).order_by('-cnt')[:10]
        
        return {
            'joueurs_actifs': joueurs_actifs,
    }
    except Evaluation.DoesNotExist:
    # always return a dict, no matter what!
        return {'joueurs_actifs':''} # an empty string
    
def redicos_importants(request): 
    #import pdb
    #pdb.set_trace()
        
    try:
        redicos_importants = Proposition.objects.all().values('redico__id', 'redico__titre').annotate(cnt=Count('redico')).order_by('-cnt')[:10]
        
        return {
            'redicos_importants': redicos_importants,
    }
    except Proposition.DoesNotExist:
    # always return a dict, no matter what!
        return {'redicos_impotants':''} # an empty string
    

def redicos_dynamiques(request): 
    #import pdb
    #pdb.set_trace()

  
    try:
        redicos_dynamiques = Proposition.objects.all().values('redico__id', 'redico__titre').annotate(cnt=Count('redico')).order_by('-cnt')[:10]
        
        return {
            'redicos_dynamiques': redicos_dynamiques,
    }
    except Proposition.DoesNotExist:
    # always return a dict, no matter what!
        return {'redicos_dynamiques':''} # an empty string
    

def dernieres_props(request):
    #import pdb
    #pdb.set_trace()    
    try:
        #dernieres_props = Proposition.objects.all().values('texte', 'redico__id').order_by('-id')[5]
        dernieres_props = Proposition.objects.all().values('texte', 'redico__id', 'sequence', 'auteur__username').order_by('-id')[:10]
    
        return {'dernieres_props': dernieres_props, }
    except Proposition.DoesNotExist:
    # always return a dict, no matter what!
        return {'dernieres_props':''} # an empty string
            

def dernieres_evals(request):
    #import pdb
    #pdb.set_trace()    
    try:
        #dernieres_props = Proposition.objects.all().values('texte', 'redico__id').order_by('-id')[5]
        dernieres_evals = Evaluation.objects.all().values('proposition__sequence', 
                                                          'proposition__redico__id', 
                                                          'proposition__texte', 
                                                          'joueur__username', 'eval').order_by('-id')[:10]
    
        return {'dernieres_evals': dernieres_evals, }
    except Evaluation.DoesNotExist:
    # always return a dict, no matter what!
        return {'dernieres_evals':''} # an empty string
            


