# -*- coding: latin-1 -*-
from django.shortcuts import render_to_response
from django.http import Http404, HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from redicos.models import Redico
from propositions.models import Proposition
from evaluations.models import Evaluation
from redicos.forms import RedicoForm
from django.db.models import Count
from django.template import RequestContext, loader
#from redicos.context_processors import redicos_importants
#from redicos.context_processors import redicos_dynamiques
from django.contrib.auth.decorators import login_required
#import pdb;

# Ajout d'un nouveau redico
@login_required
def ajout(request):
    red = Redico(createur=request.user)   
    # La forme a ete soumise 
    if request.POST:
        redform = RedicoForm(request.POST, instance=red)
        # Invalid form si user deja inscrit par ex. 
        if redform.is_valid():
            #redform = redform.save(commit=False)
            #redform.createur = request.user
            redform.save()
            return HttpResponseRedirect('/redico')

    # On a clique sur le lien 'Nouveau redico'
    else:
        redform = RedicoForm(instance=red) # Unbound form
        
    return render_to_response('redicos/ajout.html', 
                              {'form': redform, 'red':  red }) #'redico_id':redico_id})

def edit(request, redico_id=None):
    if redico_id:
        red = Redico.objects.get(id=redico_id)
        if red.createur != request.user:
            raise HttpResponseForbidden()
    else:
        red = Redico(createur=request.user)    
    
    #pdb.set_trace()
    if request.POST:
        redform = RedicoForm(request.POST, instance=red)
        # Invalid form si user deja inscrit par ex. 
        if redform.is_valid():
            #redform = redform.save(commit=False)
            #redform.createur = request.user
            redform.save()
            return HttpResponseRedirect('/redico')
    else:
        redform = RedicoForm(instance=red) # Unbound form
        
    return render_to_response('redicos/edit.html', 
                              {'form': redform, 'red': 
                               red, 'redico_id':redico_id})


# Les details d'un redico comprennent ses propositions

def detail(request, redico_id, statsdetail=None):
    #import pdb;
    #pdb.set_trace()
    try:
        red           = Redico.objects.get(pk=redico_id)
        props         = Proposition.objects.filter(redico=redico_id).order_by('-id')
        participants  = Evaluation.objects.filter(proposition__redico=redico_id).values('joueur__username').distinct()
        stats         = calcStats(redico_id)
        red_context    = {'red': red,                         
                          'props': props,                         
                          'participants': participants,                         'stats': stats                  
                          }
    except Redico.DoesNotExist:
        raise Http404
    if statsdetail:
        t = loader.get_template("redicos/statsdetail.html")
    else:
    #pdb.set_trace()
        t = loader.get_template("redicos/detail.html")
 
    c = RequestContext(request,                       
                       red_context,                       
                       #[redicos_importants,                       
                       # redicos_dynamiques]
                       )
    return HttpResponse(t.render(c)) 
    #render_to_response('redicos/detail.html',    #                              red_context,     #                              context_instance=RequestContext(request),    #                              )

# La liste des redicos et nombre de propositions
def index(request):
    # Liste des redicos et du nombre de propositions
    # r[0].proposition__count
    reds   = Redico.objects.filter(actif=True).annotate(Count('proposition',distinct=True)) \
                                 .annotate(Count('proposition__evaluation__joueur', distinct=True)).order_by('-id')
    #red_context = { 'reds': reds}   
    t = loader.get_template("redicos/index.html")
    red_context    = {'reds': reds }    
    
    c = RequestContext(request, 
                       red_context) 
                       #[redicos_importants,                       
                       # redicos_dynamiques])
                                                     
    return HttpResponse(t.render(c))
    #return render_to_response('redicos/index.html', 
    #                              red_context,  
    #                              context_instance=RequestContext(request))
                              
                               

import numpy
import numpy.ma as ma
import itertools


def calcStats(redico_id):
    
    props   = Proposition.objects.filter(redico=redico_id)
    joueurs = Evaluation.objects.filter(proposition__redico=redico_id).values('joueur_id', 'joueur__username').distinct()
    joueursList = [(joueur["joueur_id"], joueur["joueur__username"]) for joueur in joueurs]
    propsList = [prop.id for prop in props]
    
    #print(props)
    #print(joueurs)         
    
    nbProps      = len(propsList)
    tableProps   = numpy.zeros(len(propsList), int)  # <---------------------int
    nbJoueurs    = len(joueursList)
    tableJoueurs = numpy.zeros(len(joueursList), int) # <---------------------int
    
    if nbJoueurs < 2: # NbJoueurs = 1 ou 0 (evite les index out of bounds)
        extraCols=1
    else:
        extraCols = numpy.cumsum(range(1, nbJoueurs))[-1]   # Colonnes pour difference
    
    # Reserver une rangee de plus pour les stats
    # et une autre pour le nombre de masques 
    extraRan = 0
    tableEvalsPlusDiff   = numpy.ma.zeros((nbProps+extraRan, nbJoueurs+extraCols), int)
    
    tableEvalsPlusDiffM  = numpy.ma.masked_less(tableEvalsPlusDiff, 0)
    #tableEvalsPlusDiffM.fill_value(-1)
    
    #print(tableEvalsPlusDiff)
    #print(tableEvalsPlusDiffM)
    
    
    # for propositions (horz)
    for idxseq, p in enumerate(propsList):
        p = Proposition.objects.get(id=p)
        tableProps[idxseq] = p.sequence
        # for joueurs (vert)    
        for idxjou, (jid, jnom)  in enumerate(joueursList):        # (0, 2)
            # j = (2, u'Denis')
            tableJoueurs[idxjou] = jid
            try:
                e = p.evaluation_set.get(joueur=jid)
                tableEvalsPlusDiff[idxseq][idxjou] = e.eval
                #print(e, idxseq, idxjou)
            except Evaluation.DoesNotExist:
                tableEvalsPlusDiff[idxseq][idxjou] = -1  # Non /value
                #print(e, idxseq, idxjou)
    
    #print(tableEvalsPlusDiff)
    tableEvalsPlusDiffM = ma.masked_equal(tableEvalsPlusDiff, -1)
    #print(tableEvalsPlusDiffM)
    
    
    
    x = range(nbJoueurs)
    # z([1, 2, 3]) = [(0,1),(0, 2),(0,3), (1, 2),(1, 3),(2, 3)]
    # Combinaison de 2 elements pris parmi nj (nombre de joueurs)
    z  = list(itertools.combinations(x, 2))
    #print z     # [(0, 1),(0, 2),(1, 2)]
    
    # Remplir colonnes avec calcul de difference
    for ic in range(len(z)): #, c in enumerate([0,1]):
            #tableEvalsDiff[ir][ic] = abs(tableEvals[ir][z[ic][0]] - tableEvals[ir][z[ic][1]])
    
            colGauche = z[ic][0] # (0, .), (0, .), (1, .)
            colDroite = z[ic][1] # (., 1), (., 2), (., 2)
         
            tmp1 = tableEvalsPlusDiffM[:, colGauche]
            tmp2 = tableEvalsPlusDiffM[:, colDroite]
            tableEvalsPlusDiffM[:, ic+nbJoueurs] = abs(tmp1-tmp2)
    
    
    # Pour le calcul des stats, ne pas inclure la derniere rangee 
    # qui est reserve pour le bn de masques ni l'avant derniere 
    # pour la moyenne
    lstAvg = numpy.ma.average(tableEvalsPlusDiffM, axis=0)
    nbMask = ma.count_masked(tableEvalsPlusDiffM, axis=0)
    lstEvals = nbProps - nbMask
    
    
    # La liste des differences entre joueurs
    
    lstCouples = []
    for idx, zz in enumerate(z):
        j1 = joueurs[zz[0]]["joueur__username"]
        j2 = joueurs[zz[1]]["joueur__username"]
        lstCouples.append(j1[:4] + "-" + j2[:4]) # Racourcir les noms pour les tableaux
        #print(lstCouples)
    
   
    # Retourner que les stats de la table et les couples
    return [tableEvalsPlusDiffM[:, -len(z):], lstAvg[-len(z):], lstEvals[-len(z):], lstCouples]






