# -*- coding: latin-1 -*-

from django import template
from redico.evaluations.models import Evaluation
#import pdb;
#pdb.set_trace()\
    
register = template.Library()

# Fontion pour tag d'inclusion 
#@register.inclusion_tag('evaluations/liste_des_evaluateurs.html', takes_context=True)
@register.inclusion_tag('redicos/insertion_tags/evalueYN.html')
def evalueYN(red, seq, joueur):
 
    evaluateurs = Evaluation.objects.filter(proposition__redico=red, proposition__sequence=seq).values('joueur_id').distinct()
    #pdb.set_trace()
    afficher = 'Oui'
    for item in evaluateurs:
        if item['joueur_id'] == joueur:
            afficher = 'Non'
    if not joueur:
        afficher = 'Non'  
            
    return { 'afficher': afficher, 
             'red': red, 
             'seq': seq, 
             'joueur': joueur}

#@register.inclusion_tag('redicos/insertion_tags/statut.html', takes_context=True)
#def statut(context, logueOunonlogue):
#    statut = 'Oui'

#    return { 'context': context,
#             'statut': statut  }