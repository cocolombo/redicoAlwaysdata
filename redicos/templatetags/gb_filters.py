from django.template.defaultfilters import stringfilter
from django import template
import pdb

register = template.Library()

@register.filter(name='abstention')
@stringfilter
def abstention(value):

    #pdb.set_trace()   
    if value == "-1":
        return 'abs'
    else:
        return u"%s%%\n" % (value)

