# -*- coding: latin-1 -*-
# Create your views here.
from django.shortcuts import render_to_response
from redico.evaluations.models import Evaluation
from redico.propositions.models import Proposition
from redico.redicos.models import Redico
from django.db.models import Count
from redico.evaluations.forms import EvaluationForm
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.models import User
from django.template import RequestContext
from django.template import Context, Template
from django.views.generic.simple import direct_to_template


#(r'^redico/(?P<redico_id>\d+)/proposition/(?P<proposition_id>\d+)/evaluations/ajout/$',  
#                                                'redico.evaluation.views.ajout'),   
# redico/1/proposition/1/evlauations/ajout
# Model Evaluation:
#    proposition
#    redico
#    joueur
#    commentaire
#    eval
#    date-eval


from django.contrib.auth.decorators import login_required


# redico/1/proposition/1/evaluations/ajout
#(r'^redico/(?P<redico_id>\d+)/proposition/(?P<sequence_id>\d+)/evaluation/ajout/$',  
#                                           'redico.evaluations.views.ajout'),   
@login_required
def ajout(request, redico_id, sequence_id):
        prop = Proposition.objects.get(redico=redico_id, sequence=sequence_id)
        red = Redico.objects.get(pk=redico_id)  #joueur = User.objects.get(pk=request.user.id)  #evals = prop.evaluation_set.all()
        
        # la forme est soumise
        if request.POST: 
            #import pdb
            #pdb.set_trace()   
            # Véréfier si c'est déjà évalué  
            try:
                e = Evaluation.objects.get(proposition=prop, joueur=request.user)
            except Evaluation.DoesNotExist:
                form = EvaluationForm(request.POST)
                
                if form.is_valid():
                    evalform = form.save(commit=False)
                    evalform.proposition = prop  #evalform.redico = red
                    evalform.redico = redico_id  #evalform.joueur = joueur
                    evalform.joueur = request.user #pdb.set_trace()
                    evalform.save()
                    return HttpResponseRedirect('/redico/%s/' % redico_id)
                    # Redirect to listing etc.
            # C'est déjà évalué 
            else:
                #print "Déjà évalue"
                return HttpResponseRedirect('/redico/%s/' % redico_id)
                # If you get here, it exists...

        # On arrive ici en provenance du lien "Evaluer"
        else: 
            form = EvaluationForm()
        return render_to_response('evaluations/ajout.html', 
                                  {'form': form,
                                   'prop': prop,
                                   'red': red,
                                   } )




@login_required
def edit(request, redico_id, sequence_id, joueur_id, template_name='evaluations/edit.html'):
    #import pdb
    #pdb.set_trace()
    prop = Proposition.objects.get(redico=redico_id, sequence=sequence_id)
    red = Redico.objects.get(pk=redico_id)
    joueur = User.objects.get(pk=request.user.id)
    #evals = prop.evaluation_set.all()
    eval = Evaluation.objects.get(proposition__redico=redico_id, proposition__sequence=sequence_id, joueur=joueur_id)

    if request.method == 'POST':
        evalform = EvaluationForm(request.POST, instance=eval)
        if evalform.is_valid():
            evalform.save()
        return HttpResponseRedirect('/redico/%s/' 
                                        % redico_id)
    else:
        evalform = EvaluationForm(instance=eval)
        
    return render_to_response('evaluations/edit.html', 
                              {'form': evalform,
                               'prop': prop,
                               'red': red,
                               'joueur': joueur,
                               'eval': eval } )




def detail(request, evaluation_id):
    try:
        e = Evaluation.objects.get(pk=evaluation_id)
   
    except Evaluation.DoesNotExist:
        raise Http404
    
    e_context = {'e' : e}

    return render_to_response('evaluations/detail.html', 
                                  e_context,  
                                  context_instance=RequestContext(request))

def index(request):
    eval_list = Evaluation.objects.all()
    e_context = { 'eval_list': eval_list }

    return render_to_response('evaluations/detail.html', 
                                  e_context,  
                                  context_instance=RequestContext(request))

