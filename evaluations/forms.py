from django.forms import ModelForm, Textarea
from redico.evaluations.models import Evaluation

class EvaluationForm(ModelForm):
    pass

    class Meta:
        model = Evaluation
        fields = ('eval', 'commentaire')
  
        widgets = {
            'commentaire' : Textarea(attrs={'cols': 60, 'rows': 4}),
            }            

        #fieldsets = [('Optionel',  {'classes': ('collapse',), 
        #                            'fields' : ['commentaire']})],  