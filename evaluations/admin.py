from django.contrib import admin
from redico.evaluations.models import Evaluation

class EvaluationAdmin(admin.ModelAdmin):
    list_display   = ('proposition', 'joueur', 'eval', 'date_eval', 'prop_Courte')
#    list_filter    = ('joueur', 'redico', 'prop_Courte', )
    #list_filter    = ('joueur', 'prop__redico', )
    list_filter    = ('joueur', )
    ordering       = ('proposition', 'date_eval',)
    date_hierarchy = 'date_eval'

    #fieldsets = [
    #    ('Identification',                   {'fields': ['proposition']}),
    #    ('Evaluation de cette proposition',  {'fields': ['eval', 'joueur']}),
    #    ('Optionel',                         {'classes': ('collapse',),  'fields' : ['commentaire']}),        
    #    ]

    actions = ['eval_action']
 
    def eval_action(self, request, queryset):
        self.message_user(request, "Action sur model eval")

    def prop_Courte(self, obj):
        return ("%s" % obj.proposition.texte[:15])

    def red(self, obj):
        return obj.proposition.redico.id

#    def redicoId(self):
#        i = Evaluation.objects.
        
admin.site.register(Evaluation, EvaluationAdmin)


'''
    def eval_redico(self, object):
        return object.prop.redico
    
        def __unicode__(self):
            return "%s" % self.id
'''