from django.conf.urls.defaults import *
#from django.views.generic import list_detail, list, ListView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from redico import settings


admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^redico/', include('redico.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # Uncomment the next line to enable the admin:
    (r'^admin/',     include(admin.site.urls)),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^$',          include('redico.redicos.urls')),
    (r'^redico/',    include('redico.redicos.urls')),
    (r'^comment_jouer',      'django.views.generic.simple.direct_to_template', {'template': 'redicos/comment_jouer.html'}),
    (r'^redico/(?P<redico_id>\d+)/proposition', include('redico.propositions.urls')),
    #EVALUATIONS==ajout/un/tous==================================================
    # redico/1/proposition/1/evaluations/ajout
    (r'^redico/(?P<redico_id>\d+)/proposition/(?P<sequence_id>\d+)/evaluation/ajout/$',  
                                                'redico.evaluations.views.ajout'),   
    # redico/1/proposition/1/evaluation/edit/joueur_id
    (r'^redico/(?P<redico_id>\d+)/proposition/(?P<sequence_id>\d+)/evaluation/edit/(?P<joueur_id>\d+)/$',  
                                                'redico.evaluations.views.edit'),   
    # redico/1/proposition1/1/evaluations/                             
    (r'^redico/(?P<redico_id>\d+)/proposition/(?P<sequence_id>\d+)/evaluations/$',    
                                                'redico.evaluations.views.index'),

    (r'^joueur/(?P<joueur_id>\d+)/$',     'redico.joueurs.views.detail'),                 

    (r'^joueurs/$',                       'redico.joueurs.views.indexAll'),                 

    #(r'^joueur/(?P<joueur_id>\d+)/profile/$',   'redico.joueurs.views.profile'),                 
    (r'^joueurs/redico(?P<redico_id>\d+)/$',   'redico.joueurs.views.index'),                 

    #(r'^base/$',                       'redico.redicos.views.base'),
    #(r'^accounts/', include('registration.urls')),
    (r'^accounts/', include('registration.backends.default.urls')),


    (r'^static/(?P<path>.*)$', 'django.views.static.serve',  
            {'document_root': settings.STATIC_ROOT }),
)




'''
urlpatterns += patterns('',
  (r'^accounts/profile/$', direct_to_template, {'template': 'registration/profile.html'}),
  (r'^accounts/password_reset/$', password_reset, {'template_name': 'registration/password_reset.html'}),
  (r'^accounts/password_reset_done/$', password_reset_done, {'template_name': 'registration/password_reset_done.html'}),
  (r'^accounts/password_change/$', password_change, {'template_name': 'registration/password_change.html'}),
  (r'^accounts/password_change_done/$', password_change_done, {'template_name': 'registration/password_change_done.html'}),
)
              

'''
 
