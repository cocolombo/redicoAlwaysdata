# -*- coding: latin-1 -*-

from django.db import models
from django.contrib.auth.models import User
#from redico.evaluations.models import Evaluation
from redico.redicos.models import Redico

class Proposition(models.Model):
    auteur      = models.ForeignKey(User, editable=False)
    redico      = models.ForeignKey(Redico, editable=False)
    sequence    = models.IntegerField(editable=False)    # Repart a 1 pour chaque redico
    texte       = models.TextField()
    photo       = models.ImageField(blank=True, 
                                    upload_to='static/image')  # Il faut instaler PIL
    lien        = models.URLField(blank=True)
    preambule   = models.TextField(blank=True, 
                                   verbose_name='preambule')
    date_de_pub = models.DateField(verbose_name='date de publication', 
                                   auto_now_add=True)

    def name(self):
        return self.texte.split('\n', 2)[0]

    def __unicode__(self):
        return u"%s" % (self.texte)           

    def get_absolute_url(self):
        p = Proposition.objects.get(pk=self.id)
        return "/redico/%i/%i/" % self.p.redico, self.p.sequence
