from django.conf.urls.defaults import *
from redico.redicos import views 
# from redico.redicos.views import redico, redicoProps
urlpatterns = patterns('redico.propositions.views',
    #PROPOSITIONS==ajout/un/tous==========================
    
    # Une nouvelle proposition dans un redico                        
    # redico/1/proposition/ajout_edit                   
    url(r'^/ajout/$',     
                                'ajout',  
                                name='proposition-ajout'),
    
    # Editer une proposition                        
    # redico/1/proposition/1/ajout_edit                   
    url(r'^/edit/(?P<prop_id>\d+)/$',  
                                'edit',  
                                name='proposition-edit'),

    # redico/1/propositions/1/                              
    url(r'^/(?P<sequence_id>\d+)/$', 
                                'detail', 
                                name='proposition-detail'),  

    # redico/1/propositions/1/edit                              
    url(r'^/(?P<sequence_id>\d+)/edit/$', 
                                'edit', 
                                name='proposition-edit'),  

    # Toutes les propositions d'un redico        
    # redico/1/propositions                              
    url(r'^s/$',                     
                                'index',  
                                name='proposition-index'),
)