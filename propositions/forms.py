from django.forms import ModelForm, Textarea
from redico.propositions.models import Proposition

class PropositionForm(ModelForm):

    class Meta:
        model = Proposition
        #fields = ('preambule', 'texte', 'auteur', 
        # 'redico', 'proposition')
 
        widgets = {
          'texte'     : Textarea(attrs={'cols': 60, 'rows': 8}),
          'preambule' : Textarea(attrs={'cols': 60, 'rows': 10}),
        }
        #fieldsets = [ ('Optionel',  {'classes': ('collapse',), 
        #                             'fields' : ['preambule']})],  
        #                             
                                     