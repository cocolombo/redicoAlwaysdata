# -*- coding: latin-1 -*-
# propositions/views.py
from django.shortcuts import render_to_response
from propositions.models import Proposition
from evaluations.models import Evaluation
from redicos.models import Redico
from django.db.models import Count, Avg
from redico.propositions.forms import PropositionForm
from django.http import HttpResponseRedirect, Http404
from datetime import datetime
from django.http import HttpResponseForbidden, HttpResponse
from django.template import RequestContext, loader

# redico/1/propositions/ajout
# Les indices des propositions sont relative au redico 
# Ex: r1/p1. r1/p2. r2/p1, r3/p1...
# Model:
#     proposition
#     redico
#     texte
#     auteur
#     preambule
#     date_de_pub
#import pdb;
#pdb.set_trace()
 
from django.contrib.auth.decorators import login_required

@login_required
# ajout_edit/1/ pour ajout
# ajout_edit/1/1 pour edit prop 1 du red 1
def ajout(request, redico_id):
    redico = Redico.objects.get(pk=redico_id)
    prop_count   = Proposition.objects.filter(redico=redico_id).aggregate(Count('texte'))
    next_seq_id = prop_count['texte__count'] + 1 

    # edit
    prop = Proposition(auteur=request.user)
    prop.redico_id   = redico.id
    prop.sequence   = next_seq_id
    
    #import pdb;
    #pdb.set_trace()
    # On arrive ici par la soumission de la forme       
    if request.POST:
        propform = PropositionForm(request.POST, instance=prop) 
        if propform.is_valid():
            propform.auteur      = request.user
            propform.redico_id   = redico.id
            propform.sequence    = next_seq_id
            propform.date_de_pub = datetime.today() 
            propform.save()
            return HttpResponseRedirect('/redico/%s/' 
                                        % redico_id)
    # On arrive ici par le lien
    else:
        propform = PropositionForm(instance=prop)
        
    return render_to_response('propositions/ajout.html', 
                      {'form': propform,
                       'redico': redico,} )

def edit(request, redico_id, sequence_id=None):
    redico = Redico.objects.get(pk=redico_id)

    # edit
    if sequence_id:
        prop   = Proposition.objects.get(redico=redico_id, sequence=sequence_id)
        if prop.auteur != request.user:
            raise HttpResponseForbidden()
    # add
    else:
        prop = Proposition(auteur=request.user)
        
    if request.POST:
        propform = PropositionForm(request.POST, instance=prop) 
        if propform.is_valid():
            prop_count   = Proposition.objects.filter(redico=redico_id).aggregate(Count('texte'))
            next_seq_id = prop_count['texte__count'] + 1 
            propform.auteur      = request.user
            propform.redico      = redico
            propform.sequence    = next_seq_id
            propform.date_de_pub = datetime.today() 
            return HttpResponseRedirect('/redico/%s/' 
                                        % redico_id)
    else:
        propform = PropositionForm(instance=prop)
        
    return render_to_response('propositions/ajout.html', 
                      {'form': propform,
                       'redico': redico,} )


# redico/1/propositions/1/     
# Les details d'une proposition comportent ses evaluations                          
def detail(request, redico_id, sequence_id):
    try:
        #Trouver le pk a partir de redico_id et sequence_id
        prop_id = Proposition.objects.get(redico=redico_id, sequence=sequence_id)
        
        red    = Redico.objects.get(pk=redico_id)
        prop   = Proposition.objects.get(id=prop_id.id)
        ev     = Evaluation.objects.filter(proposition=prop_id)
        avg    = Evaluation.objects.filter(proposition=prop_id).aggregate(Avg('eval'))
        
        red_context    = {'red' : red,                         
                          'prop': prop,                         
                          'ev'  : ev,                         
                          'avg' : avg                  
                          }
    except Proposition.DoesNotExist:
        raise Http404
    
    t = loader.get_template("propositions/detail.html")
    c = RequestContext(request,                       
                       red_context,                       
                       )
    return HttpResponse(t.render(c)) 
    
# redico/1/propositions                              
def index(request, redico_id):
    props = Proposition.objects.filter(redico=redico_id)
    red   = Redico.objects.get(pk=redico_id)
    #prop_list2 = prop_list.annotate()
    #eval_yn = Evaluation.objects.filter(redico=redico_id, 
    #                                    proposition=proposition.id)
    return render_to_response('propositions/index.html', locals())


