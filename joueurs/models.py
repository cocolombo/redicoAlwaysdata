# MODELS
from django.contrib.auth.models import User, models

class Joueur(models.Model):  
    joueur  = models.ForeignKey(User)

    def __unicode__(self):
        return u"%s" % (self.username)

