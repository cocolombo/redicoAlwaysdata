# -*- coding: latin-1 -*-
# joueurs/views.py
# VIEWS
from django.shortcuts import render_to_response
from django.http import Http404
from redicos.models import Redico
from evaluations.models import Evaluation

from django.contrib.auth.models import User
from redicos.context_processors import redicos_importants
from redicos.context_processors import redicos_dynamiques
from django.http import HttpResponse
from django.template import RequestContext, Template, loader

def detail(request, joueur_id):
    try:
        lejoueur = User.objects.get(pk=joueur_id)
        redAuteur = Redico.objects.filter(createur=lejoueur)
        redParticipants = Evaluation.objects.filter(joueur=joueur_id).values('proposition__redico__id', 'proposition__redico__titre').distinct()
        joueur_context = { 'lejoueur':lejoueur,
                           'redAuteur':redAuteur,
                           'redParticipants':redParticipants, }
    except User.DoesNotExist:
        raise Http404
    
    t = loader.get_template("joueurs/detail.html")
    c = RequestContext(request,                       
                       joueur_context,                       
                       #[redicos_importants,                       
                       # redicos_dynamiques]
                       )
    return HttpResponse(t.render(c)) 
#                              context_instance=RequestContext(request))#    {'joueur': j,#  'ses_redicos': r})
def index(request, redico_id):
    joueur_list = User.objects.filter(is_active=True)
    joueur_context = { 'joueur_list': joueur_list }
    
    t = loader.get_template("joueurs/index.html")
 
    c = RequestContext(request,                       
                       joueur_context,                       
                       [redicos_importants,                       
                        redicos_dynamiques])
    return HttpResponse(t.render(c)) 
# context_instance=RequestContext(request))#   {'joueur_list': joueur_list})


def indexAll(request):
    joueur_list = User.objects.filter(is_active=True).order_by('-id')
    joueur_context = { 'joueur_list': joueur_list }

    t = loader.get_template("joueurs/index.html")
    c = RequestContext(request,                       
                       joueur_context,                       
                       #[redicos_importants,                       
                       #redicos_dynamiques]
                       )
    return HttpResponse(t.render(c)) 

